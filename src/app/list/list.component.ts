import {Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/map'
import {HttpService} from '../http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [HttpService]
})

export class ListComponent implements OnInit {

  dataItems: any;
  private dataUrl = 'http://localhost:3000/list';  // URL to web api

  constructor(private dataServer: HttpService ) {}
  ngOnInit() {
    this.dataServer.loadDataItems(this.dataUrl).subscribe(
      data => {
        this.dataItems = data;
        console.log(this.dataItems);
      }
    );
  }


}
