import { AppinovAngularTestPage } from './app.po';

describe('appinov-angular-test App', () => {
  let page: AppinovAngularTestPage;

  beforeEach(() => {
    page = new AppinovAngularTestPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
